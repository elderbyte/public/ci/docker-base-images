
MY_IMAGE=$1

mkdir -p /kaniko/.docker
echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"gitlab-ci-token\",\"password\":\"$CI_JOB_TOKEN\"}}}" > /kaniko/.docker/config.json


echo "Building Docker Images with kaniko in $CI_PROJECT_DIR/$MY_IMAGE ..."

for IMAGE_TAG_DIR in $CI_PROJECT_DIR/$MY_IMAGE/*/ ; do
    MY_TAG=$(basename $IMAGE_TAG_DIR)
    MY_IMAGE_TAG=$MY_IMAGE:$MY_TAG
    echo "Building Image in $IMAGE_TAG_DIR as $CI_REGISTRY_IMAGE/$MY_IMAGE_TAG"
    /kaniko/executor --context $CI_PROJECT_DIR/$MY_IMAGE/$MY_TAG --dockerfile Dockerfile --destination $CI_REGISTRY_IMAGE/$MY_IMAGE_TAG
done

