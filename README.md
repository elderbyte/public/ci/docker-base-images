# Docker Base Images


# [elderbyte/alpine-jdk-node](https://hub.docker.com/r/elderbyte/alpine-jdk-node)

The alpine JDK Node Image is the base image for ElderByte web stack, consisting of Java and node. 
Tags of the image allow for different JDK / Node versions.

Currently supported versions

registry.gitlab.com/elderbyte/public/ci/docker-base-images/alpine-jdk-node:jdk13-node12

* registry.gitlab.com/elderbyte/public/ci/docker-base-images/alpine-jdk-node:jdk13-node12
* registry.gitlab.com/elderbyte/public/ci/docker-base-images/alpine-jdk-node:jdk14-node12
* registry.gitlab.com/elderbyte/public/ci/docker-base-images/alpine-jdk-node:jdk15-node12